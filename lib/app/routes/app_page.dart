import 'package:flutter/widgets.dart';
import 'package:get/route_manager.dart';
import 'package:manage/app/binding/home_binding.dart';
import 'package:manage/app/binding/navbar_binding.dart';
import 'package:manage/app/binding/wallet/add_wallet_binding.dart';
import 'package:manage/app/routes/app_routes.dart';
import 'package:manage/app/ui/page/home/home_page.dart';
import 'package:manage/app/ui/page/navbar/navbar_page.dart';
import 'package:manage/app/ui/page/wallet/add_wallet/add_wallet.dart';

class AppPage {
  static final pages = [
    GetPage(
      name: AppRoutes.NAVBAR,
      page: () => const NavbarPage(),
      bindings: [
        NavbarBinding(),
        HomeBinding(),
      ],
    ),
    GetPage(
      name: AppRoutes.HOME,
      page: () => const HomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.ADDWALLET,
      page: () => const AddWalletPage(),
      binding: AddWalletBinding(),
    )
  ];
}
