import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:manage/app/ui/theme/app_color.dart';
import 'package:manage/app/ui/theme/app_text_theme.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'setting'.tr,
              style: headerTheme.copyWith(color: greyColor),
            ),
          ],
        ),
      ),
    );
  }
}
