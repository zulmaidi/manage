import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:manage/app/controller/home/home_controller.dart';
import 'package:manage/app/ui/page/home/widget/action_widget.dart';
import 'package:manage/app/ui/page/home/widget/overview_widget.dart';
import 'package:manage/app/ui/widget/transaction_item_widget.dart';
import 'package:manage/app/ui/widget/wallet_widget.dart';
import 'package:manage/app/ui/theme/app_color.dart';
import 'package:manage/app/ui/theme/app_text_theme.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 16),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFF6E5),
                    const Color(0xFFFCF2DF).withOpacity(0.54),
                    const Color(0xFFF8EDD8).withOpacity(0.15)
                  ],
                ),
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(32),
                  bottomRight: Radius.circular(32),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: kBottomNavigationBarHeight,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'total_balance'.tr,
                      style: subHeaderTheme,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'RP12.432.100'.tr,
                      style: headerTheme,
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  //card
                  SizedBox(
                    height: 200,
                    child: ListView.builder(
                      itemCount: 3 + 1,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return (index == 3)
                            ? Container(
                                width: 120,
                                margin: const EdgeInsets.only(right: 16),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(32),
                                  image: const DecorationImage(
                                    image: AssetImage(
                                        'lib/app/asset/image/card.png'),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                child: Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const Icon(
                                        Icons.add,
                                        color: whiteColor,
                                      ),
                                      Text(
                                        'add_wallet'.tr,
                                        style: const TextStyle(
                                          fontSize: 16,
                                          color: whiteColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : WalletWidget(index: index);
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      children: const [
                        OverviewWidget(),
                        SizedBox(
                          width: 16,
                        ),
                        OverviewWidget(),
                      ],
                    ),
                  ),
                  // const SizedBox(
                  //   height: 32,
                  // ),
                  // Padding(
                  //   padding: const EdgeInsets.symmetric(horizontal: 16),
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.start,
                  //     children: const [
                  //       ActionWidget(),
                  //       SizedBox(
                  //         width: 32,
                  //       ),
                  //       ActionWidget(),
                  //       SizedBox(
                  //         width: 32,
                  //       ),
                  //       ActionWidget(),
                  //     ],
                  //   ),
                  // )
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'last_transaction'.tr,
                    style: titleTheme,
                  ),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                    decoration: BoxDecoration(
                      color: greyColor.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Text(
                      'view_all'.tr,
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: blackColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: ListView.builder(
                itemCount: 5,
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return const TransactionItemWidget();
                },
              ),
            ),
            const SizedBox(
              height: 86,
            )
          ],
        ),
      ),
    );
  }
}
