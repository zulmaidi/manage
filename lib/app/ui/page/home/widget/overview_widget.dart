import 'package:flutter/material.dart';
import 'package:manage/app/ui/theme/app_color.dart';
import 'package:get/get.dart';

class OverviewWidget extends StatelessWidget {
  const OverviewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24),
          border: Border.all(
            color: greyColor.withOpacity(0.2),
          ),
        ),
        margin: const EdgeInsets.only(bottom: 8),
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 8,
        ),
        child: ListTile(
          contentPadding: EdgeInsets.zero,
          leading: Container(
            height: 54,
            width: 54,
            decoration: BoxDecoration(
              color: greyColor.withOpacity(0.2),
              borderRadius: BorderRadius.circular(16),
            ),
            child: const Icon(
              Icons.expand_less_outlined,
              color: blackColor,
            ),
          ),
          title: Text(
            'expenses'.tr,
            style: const TextStyle(
              fontSize: 14,
              color: greyColor,
            ),
          ),
          subtitle: const Text(
            'RP120.000',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: 16,
              color: blackColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
