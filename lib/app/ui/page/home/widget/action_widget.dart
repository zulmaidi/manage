import 'package:flutter/material.dart';
import 'package:manage/app/ui/theme/app_color.dart';
import 'package:get/get.dart';

class ActionWidget extends StatelessWidget {
  const ActionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 56,
            width: 56,
            decoration: BoxDecoration(
              color: greyColor.withOpacity(0.2),
              borderRadius: BorderRadius.circular(16),
            ),
            child: const Icon(
              Icons.history,
              color: blackColor,
            ),
          ),
          Text(
            'income'.tr,
            style: const TextStyle(fontSize: 14),
          )
        ],
      ),
    );
  }
}
