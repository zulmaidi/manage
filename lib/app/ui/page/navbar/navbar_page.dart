import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:manage/app/controller/navbar/navbar_controller.dart';
import 'package:manage/app/ui/page/debt/debt_page.dart';
import 'package:manage/app/ui/page/home/home_page.dart';
import 'package:manage/app/ui/page/navbar/widget/button_type_widget.dart';
import 'package:manage/app/ui/page/setting/setting_page.dart';
import 'package:manage/app/ui/theme/app_color.dart';
import 'package:manage/app/ui/theme/app_text_theme.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class NavbarPage extends GetView<NavbarController> {
  const NavbarPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PersistentTabView(
        context,
        controller: controller.persistentTabController,
        screens: const [
          HomePage(),
          HomePage(),
          DebtPage(),
          SettingPage(),
        ],
        decoration: NavBarDecoration(
          borderRadius: BorderRadius.circular(32),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 6,
              blurRadius: 36,
              offset: const Offset(0, 16), // changes position of shadow/
            ),
          ],
        ),
        floatingActionButton: Obx(
          () => (controller.navbarIndex.value == 0)
              ? Padding(
                  padding: const EdgeInsets.only(bottom: 74, right: 16),
                  child: GestureDetector(
                    onTap: () {
                      Get.bottomSheet(
                          SingleChildScrollView(
                            child: Container(
                              padding: const EdgeInsets.all(16),
                              decoration: const BoxDecoration(
                                color: whiteColor,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(32),
                                  topRight: Radius.circular(32),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Text(
                                    'add_transaction'.tr,
                                    style: headerTheme.copyWith(fontSize: 16),
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  const Divider(),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Obx(
                                    () => Row(
                                      children: [
                                        ButtonTypeWidget(
                                          lable: 'expenses'.tr,
                                          isSelected: controller
                                                  .transactionType.value ==
                                              'expense',
                                          onTap: () {
                                            controller
                                                .setTransactionType('expense');
                                          },
                                        ),
                                        const SizedBox(
                                          width: 16,
                                        ),
                                        ButtonTypeWidget(
                                          lable: 'income'.tr,
                                          isSelected: controller
                                                  .transactionType.value ==
                                              'income',
                                          onTap: () {
                                            controller
                                                .setTransactionType('income');
                                          },
                                        ),
                                        const SizedBox(
                                          width: 16,
                                        ),
                                        ButtonTypeWidget(
                                          lable: 'transfer'.tr,
                                          isSelected: controller
                                                  .transactionType.value ==
                                              'transfer',
                                          onTap: () {
                                            controller
                                                .setTransactionType('transfer');
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 150,
                                  )
                                ],
                              ),
                            ),
                          ),
                          isScrollControlled: true,
                          isDismissible: false);
                    },
                    child: Container(
                      height: 54,
                      width: 54,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: blackColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 6,
                            blurRadius: 36,
                            offset: const Offset(
                                0, 16), // changes position of shadow
                          ),
                        ],
                      ),
                      child: const Icon(
                        Icons.add,
                        color: whiteColor,
                      ),
                    ),
                  ),
                )
              : Container(),
        ),
        items: [
          PersistentBottomNavBarItem(
              icon: const Icon(Icons.home_rounded),
              activeColorPrimary: whiteColor,
              inactiveColorPrimary: greyColor),
          PersistentBottomNavBarItem(
            icon: const Icon(Icons.insights),
            activeColorPrimary: whiteColor,
            inactiveColorPrimary: greyColor,
          ),
          PersistentBottomNavBarItem(
              icon: const Icon(Icons.people),
              activeColorPrimary: whiteColor,
              inactiveColorPrimary: greyColor),
          PersistentBottomNavBarItem(
            icon: const Icon(Icons.settings),
            activeColorPrimary: whiteColor,
            inactiveColorPrimary: greyColor,
          ),
        ],
        onItemSelected: (index) {
          controller.navbarIndex.value = index;
          controller.persistentTabController.index = index;
        },
        confineInSafeArea: true,
        backgroundColor: Colors.black,
        handleAndroidBackButtonPress: true,
        resizeToAvoidBottomInset: true,
        stateManagement: true,
        navBarHeight: MediaQuery.of(context).viewInsets.bottom > 0 ? 0.0 : 72,
        hideNavigationBarWhenKeyboardShows: true,
        margin: const EdgeInsets.all(16.0),
        popActionScreens: PopActionScreensType.all,
        bottomScreenMargin: 0,
        navBarStyle: NavBarStyle.style6,
      ),
    );
  }
}
