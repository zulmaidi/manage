import 'package:flutter/material.dart';
import 'package:manage/app/ui/theme/app_color.dart';

class ButtonTypeWidget extends StatelessWidget {
  final String lable;
  final bool isSelected;
  final VoidCallback onTap;
  const ButtonTypeWidget({
    Key? key,
    required this.lable,
    this.isSelected = false,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: (isSelected) ? blackColor : greyColor.withOpacity(0.1),
          ),
          padding: const EdgeInsets.all(16),
          child: Text(
            lable,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: (isSelected) ? whiteColor : greyColor,
              fontSize: 16,
              fontWeight: (isSelected) ? FontWeight.w600 : FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }
}
