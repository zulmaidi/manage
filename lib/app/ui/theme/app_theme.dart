import 'package:flutter/material.dart';
import 'package:manage/app/ui/theme/app_color.dart';

final ThemeData appThemeData = ThemeData(
  primaryColor: blackColor,
  fontFamily: 'Poppins',
  scaffoldBackgroundColor: whiteColor,
);
