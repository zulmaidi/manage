import 'package:flutter/material.dart';

const Color blackColor = Color(0xFF161719);
const Color whiteColor = Colors.white;
const Color greyColor = Color(0xFF8F9AA9);
const Color redColor = Color(0xFFFD5662);
