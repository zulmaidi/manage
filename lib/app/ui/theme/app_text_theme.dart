import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:manage/app/ui/theme/app_color.dart';

const TextStyle subHeaderTheme = TextStyle(color: greyColor);
const TextStyle headerTheme = TextStyle(
  color: blackColor,
  fontSize: 24,
  fontWeight: FontWeight.w600,
);

const TextStyle titleTheme = TextStyle(
  color: blackColor,
  fontSize: 15,
  fontWeight: FontWeight.w600,
);
