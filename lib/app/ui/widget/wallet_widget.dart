import 'package:flutter/material.dart';
import 'package:manage/app/ui/theme/app_color.dart';
import 'package:manage/app/ui/theme/app_text_theme.dart';
import 'package:get/get.dart';

class WalletWidget extends StatelessWidget {
  final int index;
  const WalletWidget({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      width: Get.width - 64,
      margin: EdgeInsets.only(right: 16, left: (index == 0) ? 16 : 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(32),
        image: const DecorationImage(
          image: AssetImage('lib/app/asset/image/card.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'current_balance'.tr,
                style: const TextStyle(
                  color: whiteColor,
                ),
              ),
              Text(
                'RP12.432.100'.tr,
                style: headerTheme.copyWith(color: whiteColor),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            child: Text(
              'cash'.tr,
              style: const TextStyle(
                fontSize: 16,
                letterSpacing: 2,
                color: whiteColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}
