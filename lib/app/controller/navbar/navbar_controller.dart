import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class NavbarController extends GetxController {
  RxInt navbarIndex = 0.obs;
  RxString transactionType = 'expense'.obs;
  PersistentTabController persistentTabController =
      PersistentTabController(initialIndex: 0);

  setTransactionType(String type) {
    transactionType.value = type;
  }
}
