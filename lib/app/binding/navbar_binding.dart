import 'package:get/instance_manager.dart';
import 'package:manage/app/controller/navbar/navbar_controller.dart';

class NavbarBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NavbarController>(() => NavbarController());
  }
}
