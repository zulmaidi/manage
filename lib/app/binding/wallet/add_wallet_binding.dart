import 'package:get/instance_manager.dart';
import 'package:manage/app/controller/wallet/add_wallet_controller.dart';

class AddWalletBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AddWalletController());
  }
}
