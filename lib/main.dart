import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:manage/app/routes/app_page.dart';
import 'package:manage/app/routes/app_routes.dart';
import 'package:manage/app/translation/app_translation.dart';
import 'package:manage/app/ui/theme/app_theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
    );
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: appThemeData,
      locale: const Locale('id', 'ID'),
      getPages: AppPage.pages,
      translationsKeys: AppTranslation.translations,
      initialRoute: AppRoutes.NAVBAR,
    );
  }
}
